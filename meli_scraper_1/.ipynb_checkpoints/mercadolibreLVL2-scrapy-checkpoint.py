from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.loader.processors import MapCompose
from scrapy.selector import Selector
from scrapy.loader import ItemLoader

class Articulo(Item):
    titulo = Field()
    precio = Field()
    descripcion = Field()

class meliCrawler(CrawlSpider):
    name = "meli"
    custom_settings = {
        'USER_AGENT' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)Chrome/80.0.3987.132 Safari/537.36',
        #'CLOSESPIDER_PAGECOUNT':20
    }
    start_urls = ["https://animales.mercadolibre.cl/perros-juguetes-lanzapelotas/#applied_filter_id%3Dcategory%26applied_filter_name%3DCategor%C3%ADas%26applied_filter_order%3D2%26applied_value_id%3DMLC417979%26applied_value_name%3DLanzapelotas%26applied_value_order%3D5%26applied_value_results%3D164"]

    download_delay = 1 ## tiempo entre requerimientos

#    allowed_domains = ['https://animales.mercadolibre.cl',
#                       'https://articulo.mercadolibre.cl'] ## lista de dominios permitidos

    rules = (
        Rule(                        ## regla para desplazamiento horizontal (paginacion)
            LinkExtractor(
             allow=r'/_Desde_'
             ), follow=True
        ),
        Rule(                       ## regla para desplazamiento vertical
            LinkExtractor(
                allow = r'/MLC-'
            ), follow = True, callback = "parse_item" # Callback va solo donde quiero extraer los datos
        ),
    )  ## a donde tiene que ir y donde no. se utilizan regex

    def limpiador(self, texto):
        nuevotexto = texto.replace('\n', ' ').replace('\r', ' ').replace('\t', ' ').strip()
        return nuevotexto

    def parse_item(self, response):
        sel = Selector(response)
        item = ItemLoader(Articulo(), sel)

        item.add_xpath('titulo', '//h1[@class = "ui-pdp-title"]/text()',MapCompose(self.limpiador))
        item.add_xpath('precio', '//div[@class="andes-tooltip__trigger"]//span[@class="price-tag-fraction"][1]/text()', MapCompose(self.limpiador))
        item.add_xpath('descripcion', '//div[@class="ui-pdp-description"]/p/text()')

        yield item.load_item()